#!/usr/bin/env python3
import pandas as pd
import numpy as np
import math
from functools import reduce
from matplotlib import pyplot as plt
from iteration_utilities import deepflatten
from prodcurve import production


"""

todo:
    1. location of cities
    2. cost to trade
    3. loss in trade


"""


def getCities():
    farm={'product': 'food', 'max':200, 'efficency': 20}
    amazing_farm={'product': 'food', 'max':1000, 'efficency': 40}
    gold_in_creeks={'product': 'gold', 'max':10, 'efficency': 10}
    gold_mine={'product': 'gold', 'max':100, 'efficency': 100}
    town_square={'product': 'knowledge', 'max':10, 'efficency': 10}
    recluse={'product': 'knowledge', 'max':500, 'efficency': 10}

    cities =  [
            {'name': 'Ank',
                'population': 3,
                'health': 100,
                'happiness': 100,
                'factories': [farm, gold_in_creeks,town_square],
                'inventory': {'gold': 0, 'food': 1, 'knowledge': 1,},
                'workers': {'gold': 0, 'food': 3, 'knowledge': 0, 'trade': '0',},
                'location': [2, 5],
                },
            {'name': 'Mor Pork',
                'population': 3,
                'health': 100,
                'happiness': 100,
                'factories': [amazing_farm, gold_in_creeks,town_square],
                'inventory': {'gold': 0, 'food': 0, 'knowledge': 0,},
                'workers': {'gold': 0, 'food': 3, 'knowledge': 0, 'trade': '0',},
                'location': [82, 50],
                },
            {'name': 'Isengar',
                'population': 3,
                'health': 100,
                'happiness': 100,
                'factories': [farm, gold_mine, recluse],
                'inventory': {'gold': 0, 'food': 0, 'knowledge': 0,},
                'workers': {'gold': 0, 'food': 3, 'knowledge': 0, 'trade': '0',},
                'location': [8, 80],
                },
            {'name': 'Mordor',
                'population': 3,
                'health': 100,
                'happiness': 100,
                'factories': [farm, gold_mine, town_square],
                'inventory': {'gold': 0, 'food': 0, 'knowledge': 0,},
                'workers': {'gold': 0, 'food': 3, 'knowledge': 0, 'trade': '0',},
                'location': [52, 78],
                },
            ]

    for city in cities:
        city['capacity'] = calculate_capacity(city, things)
        #print('city', city['name'], city['capacity'])
    return cities

def getTradeRoutes(cities):
    routes = {}
    for A in cities:
        routes[A['name']] = {}
        for B in cities:
            if A != B:
                distance = math.sqrt(
                        (A['location'][0] - B['location'][0])**2 +
                        (A['location'][1] - B['location'][1])**2)
                routes[A['name']][B['name']] = {
                        'key': f"{A['name']}-{B['name']}",
                        'B': B,
                        'distance': distance,
                        'epochs': distance/100, # time taken for 1 journey
                        'spoilage': max(0.1 + distance/200, 0.9999),
                        'trade': 0,
                        }
    return routes

things = ['food', 'gold', 'knowledge']
# industries have workers
industries = ['food', 'gold', 'knowledge', 'trade']
traded_things = ['food', 'gold']

# how much can we make of each thing
def calculate_capacity(city, things):
    capacity = {thing: 0 for thing in things}
    for factory in city['factories']:
        capacity[factory['product']] += factory['max']
    return capacity

def account_phase(city):
    # decorate the city with a "enlightenment" property from 0-6
    city['enlightenment'] = min(
        math.log(max(1,city['inventory']['knowledge']), 10), 6)*100/6
    city['trade'] = {'gold': 0, 'food': 0}

def trade_phase(city, routes_dict):
    routes = routes_dict.values()
    here = city
    here['workers']['trade'] = 0
    if 'wages' in here:
        purchase_cost = here['cost']['food']
        wages = here['wages']
        # food per person in stock
        food_here = here['inventory']['food'] / here['population']
        spare_food = here['inventory']['food'] - here['population']
        stock_profit_threshold = 0.96 + (0.408 / ( food_here + 0.2))
        # allow each city to increase trade 10% per year
        last_years_trade = sum(r['trade'] for r in routes)
        city_max_trade_capacity = min(
                here['population'] * 0.001,
                1.1 * last_years_trade,
                spare_food)
        ## calculate profitability of each route
        for route in routes:
            there = route['B']
            journey_wages = wages * route['epochs']
            sale_cost = there['cost']['food']
            delivered_cost = journey_wages + purchase_cost / (1-route['spoilage'])
            # https://www.desmos.com/calculator/ngxdq7ecms
            route['profit_per_journey'] = sale_cost - delivered_cost
        sorted_routes = sorted(routes, key=lambda r: r['profit_per_journey'], reverse=True)
        for route in sorted_routes:
            there = route['B']
            if route['profit_per_journey'] >= stock_profit_threshold:
                required_food = there['population'] - there['inventory']['food']
                possible_trade = min(city_max_trade_capacity, required_food, last_years_trade * 1.2)
                last_years_trade = route['trade']
                new_opportunity = possible_trade - last_years_trade
                increase = new_opportunity * 0.5
                trade = max(0, last_years_trade + increase)
            else:
                trade = 0
            route['trade'] = trade
            profit = route['profit_per_journey'] * trade
            city_max_trade_capacity -= trade
            here['inventory']['food'] -= trade
            here['inventory']['gold'] += profit
            here['workers']['trade'] += trade * route['epochs']
            here['trade']['gold'] += profit
            here['trade']['food'] -= trade
            there['inventory']['food'] += trade
            there['inventory']['gold'] -= profit
            there['trade']['gold'] -= profit
            there['trade']['food'] += trade

def allocate_workforce_phase(city):
    population = city['population']
    # traders have been pulled off in the trade phase
    allocated_workforce = city['workers']['trade']
    # track how many people we've allocated
    # allocate worksforce accross things:
    for stuff in things:
        inventory = city['inventory'][stuff]
        workers = city['workers'][stuff]
        per_capita_stuff = inventory / population
        if stuff == 'food':
            farmers = workers
            # enlightened people do not farm
            max_farmers = population * (1 - 0.8 * city['enlightenment']*10)
            potential_farmers = max(0, max_farmers - farmers)
            # NOT ENOUGH #######################:
            # if we have not enough food put everyone on it
            if per_capita_stuff < 0.00:
                # put third spare farmers or extra 25% on food
                farmers += min(potential_farmers / 3, farmers*0.25)
            elif per_capita_stuff < 0.01:
                # who isn't working on food?
                # deploy extra 5% on food, no more than quarter of remaining
                farmers += min(potential_farmers / 4, farmers*0.05)
            elif per_capita_stuff < 0.05:
                farmers += min(potential_farmers / 5, farmers*0.02)
            elif per_capita_stuff < 0.1:
                farmers += min(potential_farmers / 10, farmers*0.01)
            elif per_capita_stuff < 0.2:
                farmers = min(farmers*1.1, population)
            # TOO MUCH #######################
            elif per_capita_stuff > 0.8:
                farmers = farmers * 0.96
            elif per_capita_stuff > 0.6:
                farmers = farmers * 0.98
            elif per_capita_stuff > 0.4:
                farmers = farmers * 0.997
            allocated_workforce = farmers
            city['workers'][stuff] = farmers
        else: # deal with the left-overs
            other_stuff = len(things) - 1 # ie how many things that aren't food
            workers = (population - allocated_workforce) / other_stuff
            city['workers'][stuff] = workers
    # allocate worksforce accross factories:
    # do a simple allocation base on ratio of max capacities (ignore efficeny!)
    for factory in city['factories']:
        stuff = factory['product']
        max_capacity = city['capacity'][stuff]
        #print('city[workers][stuff]', city['workers'][stuff] )
        factory['workers'] = city['workers'][stuff] * factory['max'] / max_capacity

def production_rate(city, stuff, workers):
    factories = [factory for factory in city['factories']
            if factory['product'] == stuff]
    total_prod = 0
    #print('factories', city['name'],stuff, factories)
    for factory in factories:
        # divide up the people amoung the factories
        allocated_people = workers / len(factories)
        total_prod += production(
                input=allocated_people,
                maximum=factory['max'],
                efficency=factory['efficency'])
    return total_prod

# calculate increase production per worker
def production_per_worker(city, stuff):
    current_workers = city['workers'][stuff]
    current_production = production_rate(city, stuff, current_workers)
    alpha = 1.01 # 1 %
    test_workers = max(current_workers + 0.5, current_workers * alpha)
    delta_prod = production_rate(city, stuff, test_workers) - current_production
    delta_workers = test_workers - current_workers
    ppw = delta_prod / delta_workers
    return ppw if ppw else current_production / current_workers if current_workers else 0

def produce_phase(city):
    for stuff in things:
        workers = city['workers'][stuff]
        production = production_rate(city, stuff, workers)
        #print('production', city['name'], stuff, production)
        city['inventory'][stuff] += max(production, 0)

def price_phase(city):
    city['cost'] = {'gold':  1}
    wages = production_per_worker(city, 'gold')
    city['wages'] = wages
    #print('wages', city['name'], wages)
    for stuff in things:
        if stuff!='gold':
            city['cost'][stuff] = production_per_worker(city, stuff) / wages

def consume_phase(city):
    for stuff in things:
        if stuff=='food':
            # city consumes 1 food for 1 population
            city['inventory'][stuff] -= max(0, city['population'])

def demography_phase(city):
    #calculate happieness
    # happiness is everyone what isn't on the farm!
    city['happiness'] = (5 + city['population'] - city['workers']['food'])*100 / city['population']
    # adjust population based on food supply
    per_capita_food = city['inventory']['food'] / city['population']
    if per_capita_food > 0.5: # lots of food, lets make babies!
        if city['happiness'] > 75:
            city['population'] *= 1.02
        elif city['happiness'] > 50:
            city['population'] *= 1.01
        elif city['happiness'] > 25:
            city['population'] *= 1.005
    elif per_capita_food < -0.1: # bad famine :-(
        city['population'] *= 0.9
    elif per_capita_food < 0.0: # moderate famine
        city['population'] *= 0.99

# entropy is a bitch
def decay_phase(city):
    # old food rots
    per_capita_food = city['inventory']['food'] / city['population']
    if per_capita_food > 3: # lots of food, it won't last in storage
        city['inventory']['food'] *= 0.5
    if per_capita_food > 2: # lots of food, it won't last in storage
        city['inventory']['food'] *= 0.6
    elif per_capita_food > 1: # lots of food, it won't last in storage
        city['inventory']['food'] *= 0.7


def describe(city):
    return f"{city['name']:>10} pop: {city['population']:>5.1f} "+ (
            ' '.join([f"{stuff}: {city['inventory'][stuff]:5.0f} "
                for stuff in things])
              )

def make_log_record(city, epoch):
    return {
        "epoch": epoch,
        "city": city['name'],
        "population": city['population'],
        "happiness": city['happiness'],
        "enlightenment": city['enlightenment'],
        "wages": city['wages'],
        **{s: city['inventory'][s] for s in things},
        **{f'{s}_workers': city['workers'][s] for s in industries},
        **{f'{s}_trade': city['trade'][s] for s in traded_things},
        #**{f'{s}_production': city['production'][s] for s in things},
        **{f'{s}_cost': city['cost'][s] for s in things},
            }

def run(cities, epochs=200, reports=5):
    log = []
    reporting_interval = epochs/reports if reports else False
    all_routes = getTradeRoutes(cities)
    for epoch in range(1, epochs+1):
        reporting = reports and ((epoch / reporting_interval) % 1 == 0)
        if reporting:
            print(epoch)
        for city in cities:
            account_phase(city)
            trade_phase(city, all_routes[city['name']])
            allocate_workforce_phase(city)
            produce_phase(city)
            consume_phase(city)
            demography_phase(city)
            price_phase(city)
            decay_phase(city)
            if reporting:
                print(describe(city))
            log.append(make_log_record(city, epoch))
    if reports:
        print('done', epoch)
    df = pd.DataFrame(log)
    return df

# nice little grid plotter
def plot(df, plots):
    c = pd.pivot_table(df,
            values=list(deepflatten([p['traces'] for p in plots.values()], ignore=str)),
            index=['epoch', 'city'],
            aggfunc=np.mean).unstack(level=1)
    # c = c[values] # reorder columns
    cities = df['city'].unique()

    fig, axes = plt.subplots(len(plots), len(cities))
    x = c.index
    for row, label in enumerate(plots):
        axes[row,0].set_ylabel(label)
        for col, city in enumerate(cities):
            ax = axes[row, col]
            plot = plots[label]
            traces = plot['traces']
            values = traces if isinstance(traces, list) else [traces]
            for value in values:
                trace_label=value.replace(label.lower(), '').replace('_',' ').title()
                y = c[value, city]
                ax.plot(x,y, label=trace_label)
                if 'scale' in plot:
                    ax.set_ylim(plot['scale'][0], plot['scale'][1])
            if len(values) > 1:
                ax.legend()

    for y in range(len(cities)):
        axes[len(plots)-1][y].set_xlabel(cities[y])
    return axes

if __name__ == '__main__':
    cities = getCities()[0:4]
    df = run(cities, epochs=500)
    df['total_workers'] = df.gold_workers + df.food_workers + df.knowledge_workers
    df['pop_change'] = df.population - df.total_workers
    df['food_per_person'] = df.food / df.population
    df['food_per_wage'] = df.food_cost / df.wages
    # df['peasant_fraction'] = df.food_workers # / df.population
    plots = {
            'Population': {'traces': 'population', },
            'Stuff': {'traces':['food', 'gold', 'knowledge'], },
            'Food/Person': {'traces':'food_per_person', 'scale': (0,5)},
            'Food/Wage': {'traces':'food_per_wage', },
            # 'Happiness': {'traces': 'happiness', },
            'Wages': {'traces': 'wages', },
            'Workers': {'traces': ['food_workers', 'gold_workers', 'knowledge_workers', 'trade_workers'], },
            'Cost': {'traces': ['food_cost', 'gold_cost', 'knowledge_cost'], },
            'Trade': {'traces': ['food_trade', 'gold_trade'], },
            # 'Enlightenment': {'traces': 'enlightenment', },
        }
    plot(df, plots)
    # print('df.dtypes', df.dtypes)
    plt.show()

