import math

def production(input, maximum, efficency=1):
    a=2
    b=2
    x=input * efficency / 200
    return maximum * ((a+1)/(1+a*math.exp(-3.1*x/b))-1)/2

def print_table():
    efficencies = range(10, 110, 10)
    print("max input efficency")
    print("        "+''.join([f'{e:6.0f}' for e in efficencies]))
    for max in [10, 20, 100]:
        for input in [1, 2, 3, 4, 5, 8, 10, 20, 40, 100]:
            print(f'{max:>3} {input:>4} ' +
            ''.join([f'{production(input, max, e):6.2f}' for e in efficencies]))

if __name__ == '__main__':
    print_table()
